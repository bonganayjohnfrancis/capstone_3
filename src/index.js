import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';



//ReactJs does not like rendering two adjacent elements. Instead the adjacent elements must be wrapped by a parent element or React Fragments (<>...</>)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  
    <App />
  
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
