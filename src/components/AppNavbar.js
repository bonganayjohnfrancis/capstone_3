import React, { Component } from 'react';
import { Navbar, NavDropdown, Form, FormControl, Button, Nav } from 'react-bootstrap';
import { Link} from "react-router-dom";
import Container from 'react-bootstrap/Container';

import { BiUserCircle,BiSearch,BiCartAlt } from "react-icons/bi";

import '../App.css';
export default class NavbarComp extends Component {
    render() {
        return (
                <div>
                <Navbar  collapseOnSelect className="simple-linear" variant="light" expand="lg" color="=black" sticky="top">
                      <Container fluid>
                      <img src="anime_figurine.png" height={50} width={50} />
                        <Navbar.Brand href="/">アニメの置物</Navbar.Brand>
                      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                      <Navbar.Collapse id="responsive-navbar-nav">
                        
                        
                      <Nav className="d-flex ms-auto">

                          <Nav.Link as={Link} to="/cart"><BiCartAlt size={25} /></Nav.Link>
                            
                            <Nav.Link as={Link} to="/login">< BiUserCircle size={25} /></Nav.Link>
                          
                        </Nav>
                        <Nav >
                          <Form className ="d-flex" >
                            <FormControl
                              type="search"
                              placeholder="Search"
                              className="me-2"
                              aria-label="Search"
                            />
                            <Button variant="outline-info">Search</Button>
                          </Form>
                          </Nav>
                      </Navbar.Collapse>
                      </Container>
                    </Navbar>

                  
                </div>
                
        )
    }
}